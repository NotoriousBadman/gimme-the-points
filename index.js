/* eslint-disable camelcase */
import os from 'os';
import fs from 'fs';
import path from 'path';
import { spawn } from 'child_process';
import { getMaxSpawns } from './src/helpers.js';
// get working directory
const dirname = process.cwd();
const startApp = path.join(dirname, 'src', 'start.js');

if (!fs.existsSync('./stats.json')) {
  fs.writeFileSync('./stats.json', '{}', 'utf8');
}
const statsContent = fs.readFileSync('./stats.json', 'utf8');
const stats = JSON.parse(statsContent);

const jsonContent = fs.readFileSync('./accounts.json', 'utf8');
const accounts = JSON.parse(jsonContent);

let numCores = getMaxSpawns() || 1;
if (numCores > os.cpus().length || !numCores) {
  numCores = 1;
}

const childProcesses = [];
let runningProcesses = 0;
const processQueue = [];

function runNextProcess() {
  if (runningProcesses >= numCores || processQueue.length === 0) {
    return;
  }
  runningProcesses += 1;
  const nextProcess = processQueue.shift();
  const child = spawn(nextProcess.command, nextProcess.args, nextProcess.options);
  childProcesses.push(child);
  child.on('exit', () => {
    runningProcesses -= 1;
    runNextProcess();
  });
}

function spawnProcess(command, args, options) {
  processQueue.push({ command, args, options });
  runNextProcess();
}

function shouldRun(account, platform) {
  if (account in stats) {
    if (platform === 'mobile' && stats[account].level === 1) return false;
    const { last_ran, incomplete_tasks } = stats[account];
    const earnedPoints = stats[account][platform].match(/^\d+/)[0];
    const totalPoints = stats[account][platform].match(/\d+$/)[0];
    if (earnedPoints === totalPoints
      && last_ran.slice(0, 10) === new Date().toISOString().slice(0, 10)
      && incomplete_tasks === 0
    ) return false;
    return true;
  }
  return true;
}

function start() {
  for (let i = 0; i < accounts.length; i += 1) {
    if (shouldRun(accounts[i].email, 'desktop')) {
      spawnProcess('node', [startApp, 'desktop', accounts[i].email, accounts[i].password], { detached: false, stdio: 'inherit' });
    }
  }
  for (let i = 0; i < accounts.length; i += 1) {
    if (shouldRun(accounts[i].email, 'mobile')) {
      spawnProcess('node', [startApp, 'mobile', accounts[i].email, accounts[i].password], { detached: false, stdio: 'inherit' });
    }
  }
}

start();

process.on('SIGINT', () => {
  console.log('Received SIGINT signal, terminating child processes...');
  childProcesses.forEach((child) => {
    child.kill('SIGTERM');
  });
  process.exit();
});
