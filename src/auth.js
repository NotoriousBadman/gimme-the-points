import { By, Key } from 'selenium-webdriver';

import {
  locateElementInteract, locateElementError, waitAnimation, consentChecks,
} from './helpers.js';

const REWARDSURL = 'https://rewards.bing.com/';

export default async function bingLogin(email, password, driver) {
  console.log(`${email.match(/^[^@]+/)} logging in`);
  await driver.get(REWARDSURL);
  await consentChecks(driver);
  await locateElementInteract(By.css('#raf-signin-link-id'), undefined, async (element) => { await element.click(); }, driver);

  await locateElementInteract(By.name('loginfmt'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:loginfmt`);
    await element.sendKeys(email, Key.RETURN);
    await locateElementError(By.id('usernameError'), undefined, 'Unable to login. Please check your email and password and try again.', driver);
    await waitAnimation();
  }, driver);

  await locateElementInteract(By.name('passwd'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:passwd`);
    await element.sendKeys(password, Key.RETURN);
    await locateElementError(By.id('usernameError'), undefined, 'Unable to login. Please check your email and password and try again.', driver);
    await waitAnimation();
  }, driver);
  // 2fa
  await locateElementError(By.id('idDiv_SAOTCS_Title'), undefined, 'Unable to login due to account security. Please disable extra account verification methods (2FA) and try again.', driver);
  // lock check
  await locateElementError(By.id('StartHeader'), undefined, 'ACCOUNT LOCKED!', driver);
  // info accurate check
  await locateElementInteract(By.id('iRemindMeLater'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:iRemindMeLater`);
    element.click();
    await waitAnimation();
  }, driver);
  // security info
  await locateElementInteract(By.id('iLooksGood'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:iLooksGood`);
    await element.click();
    await waitAnimation();
  }, driver);
  // updating terms
  await locateElementInteract(By.id('iLearnMoreLink'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:iLearnMoreLink`);
    await element.click();
    await waitAnimation();
  }, driver);
  // stay signed in
  await locateElementInteract(By.id('KmsiCheckboxField'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:KmsiCheckboxField`);
    await element.click();
    await waitAnimation();
  }, driver);
  // stay signed in
  await locateElementInteract(By.id('idSIButton9'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:idSIButton9`);
    await element.click();
    await waitAnimation();
  }, driver);
  // protect check
  await locateElementInteract(By.id('ishowSkip'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:ishowSkip`);
    await element.click();
    await waitAnimation();
  }, driver);
  // break free from password check
  await locateElementInteract(By.id('iCancel'), undefined, async (element) => {
    console.log(`${driver.gtp.user}:iCancel`);
    await element.click();
    await waitAnimation();
  }, driver);
  // recover check
  await locateElementError(By.id('identityPageBanner'), undefined, 'Please add recovery email or phone number to the account', driver);
}
