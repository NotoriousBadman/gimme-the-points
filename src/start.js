import { WebDriver } from 'selenium-webdriver';
import createBrowser from './driver.js';
import bingLogin from './auth.js';
import setStats from './stats.js';
import search from './search.js';
import { startDashboard } from './dashboard.js';
import punchcards from './punchcards.js';
import {
  logError,
  updateStats,
  consentChecks,
  getSeleniumTimeouts,
  setViewport,
  getDriverConfig,
} from './helpers.js';

const args = process.argv.slice(2);

async function blankPageCheck(driver) {
  const body = await driver.executeScript('return document.body.innerHTML');
  if (body === '<pre></pre>') return true;
  return false;
}

const originalGet = WebDriver.prototype.get;
Object.defineProperty(WebDriver.prototype, 'get', {
  async value(url) {
    await originalGet.call(this, url);
    let count = 0;
    let isPageBlank = await blankPageCheck(this);
    if (isPageBlank) {
      while (count < 3 && isPageBlank) {
        console.log(`Blank page found, getting url again attempt ${count + 1}`);
        await originalGet.call(this, url);
        isPageBlank = await blankPageCheck(this);
        if (!isPageBlank) break;
        count += 1;
      }
    }
  },
});

// track some banner and cookie stats to prevent checking multiple times
function setGtpInfo(user, platform) {
  return {
    user,
    platform,
    states: {
      wcpConsentBannerCtrl: false, // cookies
      bnp_btn_accept: false, // cookies 2
      extra_login: false, // logged in 2 times
      bnp_hfly_cta2: false, // flyout bing wallpaper banner
    },
  };
}

async function start(platform, user, pass) {
  const driverConfig = getDriverConfig();
  const driver = await createBrowser(platform, driverConfig).catch(async (err) => {
    console.error(err);
    await logError(err);
    throw err;
  });
  await driver.manage().setTimeouts(getSeleniumTimeouts());
  driver.gtp = setGtpInfo(user, platform);
  await setViewport(driver);
  try {
    await bingLogin(user, pass, driver);
  } catch (err) {
    await logError(err, driver);
    if (driver) await driver.quit();
    throw err;
  }
  try {
    await consentChecks(driver);
    await setStats(driver);
    await search(driver.gtp.stats[platform], driver);
    if (platform === 'desktop') await startDashboard(driver);
    if (platform === 'desktop') await punchcards(driver);
    await setStats(driver);
    await updateStats(driver.gtp.user, driver.gtp.stats, driver.gtp.tasks);
  } catch (err) {
    await logError(err, driver);
    if (driver) await driver.quit();
    throw err;
  }
  console.log(`${driver.gtp.user}:done ${platform}`);
  if (driver) await driver.quit();
}
// email pass platform
await start(args[0], args[1], args[2]);
