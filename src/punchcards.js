import { By } from 'selenium-webdriver';
import {
  waitAnimation,
  tabClose,
  tabGetNew,
  consentChecks,
  logError,
} from './helpers.js';
import { checkTask } from './dashboard.js';

function pauseSlides(driver) {
  const pauseButton = driver.findElement(By.xpath("//button[contains(@class,'c-action-toggle c-glyph')]"));
  if (pauseButton) pauseButton.click();
}

async function getIncompleteCards(cards) {
  const incomplete = [];
  for (let index = 0; index < cards.length; index += 1) {
    const card = cards[index];
    const checkMarks = await card.findElements(By.className('mee-icon-StatusCircleCheckmark'));
    const taskIconContainers = await card.findElements(By.className('mee-icon mee-icon-StatusCircleOuter ng-scope'));
    if (checkMarks.length !== taskIconContainers.length) {
      incomplete.push(card);
    }
  }
  return incomplete;
}

async function getCards(driver) {
  const cards = await driver.findElements(By.xpath('//li[contains(@ng-repeat, "item")]/a'));
  const incompleteCards = await getIncompleteCards(cards);
  const linksToReturn = await Promise.all(incompleteCards.map(async (card) => {
    const link = await card.getAttribute('href');
    return link;
  })).then((links) => links.filter((link) => !link.includes('xbox')));
  return linksToReturn;
}

async function completeCardTasks(driver) {
  const offers = await driver.findElements(By.className('offer-cta'));
  const links = offers.map((offer) => offer.findElement(By.xpath('..//a')));
  for (let i = 0; i < links.length; i += 1) {
    const link = links[i];
    await link.click();
    await tabGetNew(driver);
    await consentChecks(driver);
    await waitAnimation();
    await checkTask(driver);
    await tabClose(driver);
    await waitAnimation();
  }
}
function setCardStats(driver, cards) {
  console.log('rannning');
  driver.gtp.stats.punchcards = cards.length;
  console.log(cards.length);
}
export default async function punchcards(driver) {
  try {
    pauseSlides(driver);
    const cards = await getCards(driver);
    setCardStats(driver, cards);
    for (let i = 0; i < cards.length; i += 1) {
      const card = cards[i];
      await driver.executeScript(`window.open('${card}','_blank');`);
      await tabGetNew(driver);
      await waitAnimation();
      await completeCardTasks(driver);
      await tabClose(driver);
      console.log(`completed punchcard${i}`);
      await waitAnimation();
      driver.gtp.stats.punchcards - 1;
    }
  } catch (err) {
    console.error(err);
    await logError(err, driver);
  }
}
