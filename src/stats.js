/* eslint-disable no-param-reassign */
import { By } from 'selenium-webdriver';
import { locateElementInteract, waitAnimation, consentChecks } from './helpers.js';

async function getLevel(driver) {
  const levelElement = await driver.executeScript('return document.querySelectorAll(\'.level\')[0]');
  const levelElement2 = await driver.executeScript('return document.querySelectorAll(\'.profileDescription\')[0]');
  if (levelElement) {
    return levelElement.getAttribute('innerText');
  } if (levelElement2) {
    return levelElement2.getAttribute('innerText');
  }
  return null;
}

async function createTasks(driver) {
  const tasks = { incomplete: [], complete: [] };
  await driver.findElements(By.xpath('//span[contains(@class, "mee-icon-AddMedium")]')).then((incompleteTasks) => {
    incompleteTasks.forEach((task) => tasks.incomplete.push(task));
  });
  await driver.findElements(By.xpath('//span[contains(@class, "mee-icon-HourGlass")]')).then((partialTasks) => {
    partialTasks.forEach((task) => tasks.incomplete.push(task));
  });
  await driver.findElements(By.xpath('//span[contains(@class, "mee-icon-SkypeCircleCheck")]')).then((completeTasks) => {
    completeTasks.forEach((task) => tasks.complete.push(task));
  });
  return tasks;
}

async function createStats(level, driver) {
  if (!level) return console.log('no level found');
  const statObj = {};
  const points = await driver.executeScript('return document.querySelectorAll(\'.pointsDetail.c-subheading-3\')');
  let pcIndex = 1;
  let mobileIndex = 2;
  let edgebonusIndex = 3;
  if (points.length < 4 && level.match(/\d$/)[0] === '2') {
    pcIndex = 0;
    mobileIndex = 1;
    edgebonusIndex = 2;
  }
  // eslint-disable-next-line default-case
  switch (level.match(/\d$/)[0]) {
    case '1':
      statObj.level = 1;
      statObj.desktop = await points[1].getText();
      statObj.edgebonus = await points[2].getText();
      statObj.mobile = null;
      break;
    case '2':
      statObj.level = 2;
      statObj.desktop = await points[pcIndex].getText();
      statObj.mobile = await points[mobileIndex].getText();
      statObj.edgebonus = await points[edgebonusIndex].getText();
      break;
  }
  statObj.streak = await driver.executeScript('return document.querySelector(\'#streakToolTipDiv > p > mee-rewards-counter-animation > span\').innerText');
  statObj.balance = await driver.executeScript('return document.querySelector(\'#balanceToolTipDiv > p > mee-rewards-counter-animation > span\').innerText');
  statObj.daily = await driver.executeScript('return document.querySelector(\'#dailypointToolTipDiv > p > mee-rewards-counter-animation > span\').innerText');
  statObj.punchcards = 0;
  return statObj;
}

export default async function setStats(driver) {
  try {
    await driver.get('https://rewards.bing.com/pointsbreakdown');
  } catch (err) {
    if (err.name === 'TimeoutError') {
      await driver.executeScript('location.reload()');
    } else {
      // Handle other errors
      throw err;
    }
  }
  await waitAnimation();
  await consentChecks(driver);
  await locateElementInteract(By.id('userPointsBreakdown'), undefined, async () => { }, driver);
  const level = await getLevel(driver);
  const stats = await createStats(level, driver);
  const tasks = await createTasks(driver);
  await waitAnimation();
  await locateElementInteract(By.className('glyph-cancel'), undefined, async (element) => { await element.click(); }, driver);
  driver.gtp.stats = stats;
  driver.gtp.tasks = tasks;
  return driver;
}
