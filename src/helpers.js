import { until, By } from 'selenium-webdriver';
import * as fs from 'node:fs/promises';

const config = await import('../config.json', { assert: { type: 'json' } });
// const stats = await import('./stats.json', { assert: { type: 'json' } }).catch(() => { console.log('stats file does not exist') })


/**
 * This function will throw a given error if an element is found. Use when expecting an element to not be found and need to throw an error if it is found.
 * @param {By} finder
 * @param {number} time
 * @param {string} error
 * @param {WebDriver} driver
 * @returns {Promise<void>}
 */
export async function locateElementError(finder, time = config.default.driver_config.element_wait, error, driver) {
  if (!driver) throw new Error('No driver');
  const url = await driver.getCurrentUrl();
  try {
    const element = await driver.wait(until.elementLocated(finder), time);
    if (element) throw new Error(error);
    return;
  } catch (err) {
    if (err.name == 'TimeoutError') return; //element not found

    if (url !== 'undefined') err.url = url;
    throw new Error(err);
  }
}

/**
 * Locate an element, check if is in view, scroll to it, and then interact with it with a given callback function.
 * @param {By} finder 
 * @param {number} time 
 * @param {callback} action 
 * @param {WebDriver} driver 
 * @param  {...any} args 
 * @returns {callbackFn}
 */
export async function locateElementInteract(finder, time = config.default.driver_config.element_wait, action, driver, ...args) {
  if (!driver) throw new Error('No active driver given to locateElementInteract');
  let url;
  try {
    url = await driver.getCurrentUrl();
  } catch (err) {
    if (err.name == 'TimeoutError') return; //element not found
    if (url !== 'undefined') err.url = url;
    throw err;
  }
  try {
    const element = await driver.wait(until.elementLocated(finder), time);
    await driver.wait(until.elementIsVisible(element), time);
    let elementIsInView = await driver.executeScript(`
          var element = arguments[0];
          var rect = element.getBoundingClientRect();
          return (
              rect.top >= 0 &&
              rect.left >= 0 &&
              rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && 
              rect.right <= (window.innerWidth || document.documentElement.clientWidth)
          );
      `, element);
    if (!elementIsInView) {
      await driver.executeScript(`arguments[0].scrollIntoView();`, element);
    }
    return await action(element);
  } catch (err) {
    if (err.name == 'TimeoutError') return; //console.log(`Element not found: ${finder}`)
    //if (err.name == 'TypeError') return //element not found
    if (url !== 'undefined') err.url = url;
    throw err;
  }
}

//write error to file with user and timestamp
export async function logError(err, driver) {
  let string = '';
  if (driver?.gtp.user) string += `${driver.gtp.user} `;
  string += `${new Date().toISOString().slice(0, -5)}`;
  if (err.name) string += ` (${err.name}: ${err.message}`;
  if (err.url) string += ` ${err.url})`;
  if (err.stack) string += `\n${err.stack}`;
  if (err.remoteStacktrace) string += `\n${err.remoteStacktrace}`;
  fs.appendFile('error.log', string + '\n');
}


export async function waitAnimation(time = config.default.driver_config.animation_wait) {
  await new Promise(r => setTimeout(r, time));
}

/**Write stats to file. move this?
* @param {string} user
* @param {object} stats
* @param {object} tasks
*/
export async function updateStats(user, stats, taskArray) {
  try {
    const fileData = await fs.readFile('stats.json', 'utf8');
    console.log('updating stats for ' + user);
    let obj = await JSON.parse(fileData);
    obj[user] = stats;
    obj[user].incomplete_tasks = taskArray.incomplete.length;
    obj[user].last_ran = new Date().toISOString().slice(0, -5);
    const json = JSON.stringify(sortObject(obj), null, 2);
    await fs.writeFile('stats.json', json, 'utf8', (err) => {
      if (err) console.error(err);
    });
  }
  catch (err) {
    console.log('error');
    console.log(err);
  }
}
//return max number of spawns from config
export function getMaxSpawns() {
  return config.default.driver_config.max_spawns;
}

//sets the viewport of the driver based on the platform
//move this to more appropriate location
export async function setViewport(driver) {
  if (driver.gtp.platform == 'desktop') {
    await driver.manage().window().setRect({
      width: config.default.driver_config.desktop_width,
      height: config.default.driver_config.desktop_height
    });
  } else if (driver.gtp.platform == 'mobile') {
    await driver.manage().window().setRect({
      width: config.default.driver_config.mobile_width,
      height: config.default.driver_config.mobile_height
    });
  }
}
//used to reorder stats file in update stats function
function sortObject(myObject) {
  let keys = Object.keys(myObject);
  keys.sort(function (a, b) {
    return new Date(myObject[a].last_ran) - new Date(myObject[b].last_ran);
  });

  let sortedObject = {};
  for (let key of keys) {
    sortedObject[key] = myObject[key];
  }
  return sortedObject;
}
//returns random geo from config. move to more appropriate location?
export function getRandomGeo() {
  const geos = config.default.search_config.query_geo;
  const randomGeo = geos[Math.floor(Math.random() * geos.length)];
  return randomGeo;
}

//closes tab if needs to and then focuses on most recent tab
export async function tabClose(driver, handle) {
  const oldHandles = await driver.getAllWindowHandles();
  if (handle) {
    await driver.switchTo().window(handle).close();
  } else {
    await driver.close();
  }
  const newHandles = await driver.getAllWindowHandles();
  const closed = oldHandles.length === newHandles.length;
  if (closed) {
    await driver.executeScript('window.close();');
  }
  await tabGetNew(driver);
  return;
}
//get most recent tab
export async function tabGetNew(driver) {
  const handles = await driver.getAllWindowHandles();
  await driver.switchTo().window(handles[handles.length - 1]);
}
//get tab by index
export async function tabGet(index, driver) {
  const handles = await driver.getAllWindowHandles();
  await driver.switchTo().window(handles[index]);
}
//only used in dashboard? move?
export function generateRandomArray(max) {
  // Create an array of numbers from 1 to max
  const numbers = Array.from({ length: max }, (v, i) => i);
  // Shuffle the array using the Fisher-Yates shuffle algorithm
  for (let i = numbers.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [numbers[i], numbers[j]] = [numbers[j], numbers[i]];
  }
  return numbers;
}
//return config options for selenium timeouts
export function getSeleniumTimeouts() {
  return { pageLoad: config.default.driver_config.page_load, script: config.default.driver_config.script_load, implicit: config.default.driver_config.implicit_load };
}

//set driver from config.json or env variable defaults to firefox
export function setDriver() {
  const browser = process.env.DRIVER || config.default.driver_config.driver || 'firefox';
  return browser;
}

//get driver options from config.json like headless, useragent, etc
export function getDriverConfig() {
  const options = config.default.driver_config;
  options.driver = setDriver();
  return options;
}

//check for and click various cookie/consent and ad banner elements that can appear while attempting completion. 
//sets a state in driver.gtp.states to prevent waiting/looking for the same element check times
export async function consentChecks(driver) {
  await waitAnimation();
  if (!driver.gtp.states.extra_login) await locateElementInteract(By.css('.identityOption > a:nth-child(2)'), undefined, async (element) => {
    await element.click();
    await waitAnimation();
    driver.gtp.states.extra_login = true;
    console.log(driver.gtp.user + ':extra_login clicked');
  }, driver);
  if (!driver.gtp.states.extra_login) await locateElementInteract(By.className('id_button'), undefined, async (element) => {
    await element.click();
    await waitAnimation();
    driver.gtp.states.extra_login = true;
    console.log(driver.gtp.user + ':extra_login clicked');
  }, driver);
  if (!driver.gtp.states.extra_login) await locateElementInteract(By.id('id_a'), undefined, async (element) => {
    await element.click();
    await waitAnimation();
    driver.gtp.states.extra_login = true;
    console.log(driver.gtp.user + ':extra_login clicked');
  }, driver);
  if (!driver.gtp.states.wcpConsentBannerCtrl) await locateElementInteract(By.xpath('//*[@id="wcpConsentBannerCtrl"]/div[2]/button[1]'), undefined, async (element) => {
    await element.click();
    driver.gtp.states.wcpConsentBannerCtrl = true;
    console.log(driver.gtp.user + ':wcpConsentBannerCtrl clicked');
    await waitAnimation();
  }, driver);
  if (!driver.gtp.states.bnp_btn_accept) await locateElementInteract(By.id('bnp_btn_accept'), undefined, async (element) => {
    await element.click();
    await waitAnimation();
    driver.gtp.states.bnp_btn_accept = true;
    console.log(driver.gtp.user + ':bnp_btn_accept clicked');
  }, driver);
  if (!driver.gtp.states.bnp_hfly_cta2) await locateElementInteract(By.id('bnp_hfly_cta2'), undefined, async (element) => {
    await waitAnimation();
    await element.click();
    driver.gtp.states.bnp_hfly_cta2 = true;
    console.log(driver.gtp.user + ':bnp_hfly_cta2 clicked');
    await waitAnimation();
  }, driver);

}