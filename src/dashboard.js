import { By, until } from 'selenium-webdriver';
import {
  waitAnimation,
  locateElementInteract,
  tabClose,
  tabGetNew,
  tabGet,
  generateRandomArray,
  consentChecks,
} from './helpers.js';

// when written like 1 o 10
async function getQuizLength(finder, driver) {
  let length;
  await locateElementInteract(finder, undefined, async (element) => {
    const text = await element.getText();
    const res = text.match(/(\d+)/g);
    length = res[1] - (res[0] - 1);
  }, driver);
  return length;
}

// length & progress is measured by the number of filled circles
async function getRewardsQuizLength(driver) {
  const total = await driver.executeScript('return document.querySelector(\'#rqHeaderCredits\').children.length');
  const filled = await driver.executeScript('return document.querySelectorAll(\'.filledCircle\').length');
  return total - (filled - 1);
}

async function startSimpleQuiz(driver) {
  const quizLength = await getQuizLength(By.className('FooterText0'), driver);
  console.log(`${driver.gtp.user}:Simple quiz length ${quizLength}`);
  for (let index = 0; index < quizLength; index += 1) {
    const choice = `ChoiceText_${index.toString()}_${Math.floor(Math.random() * 3).toString()}`;
    await locateElementInteract(By.id(choice), undefined, async (element) => {
      await element.click();
    }, driver);
    await waitAnimation();
    await consentChecks(driver);
    try {
      await locateElementInteract(By.name('submit'), undefined, async (element) => {
        await element.click();
      }, driver);
      await waitAnimation();
    } catch (err) {
      if (err.name === 'TimeoutError') {
        await driver.executeScript('location.reload()');
      } else {
        // Handle other errors
        throw err;
      }
    }
  }
}

async function startPoll(driver) {
  const option = `btoption${Math.floor(Math.random() * 2).toString()}`;
  console.log(`${driver.gtp.user}:Poll option ${option}`);
  await locateElementInteract(By.id(option), undefined, async (element) => {
    await element.click();
  }, driver);
  await waitAnimation();
}

async function startWhatsOnTop(driver) {
  const quizLength = await getQuizLength(By.className('bt_Quefooter'), driver);
  console.log(quizLength);
  console.log(`${driver.gtp.user}:Whats on top quiz length ${quizLength}`);
  for (let index = 0; index < quizLength; index += 1) {
    await consentChecks(driver);
    const choice = `rqAnswerOption${Math.floor(Math.random() * 2).toString()}`;
    try {
      await locateElementInteract(By.id(choice), undefined, async (element) => {
        await element.click();
      }, driver);
    } catch (err) {
      if (err.name === 'TimeoutError') {
        await driver.executeScript('location.reload()');
      } else {
        // Handle other errors
        throw err;
      }
    }

    if (index === quizLength - 1) break;
    const element = await driver.findElement(By.id(choice));
    await driver.wait(until.elementIsVisible(element), 30000);
  }
}

async function startRewardsQuiz(driver) {
  await waitAnimation();
  await consentChecks(driver);
  await locateElementInteract(By.id('rqStartQuiz'), undefined, async (element) => {
    if (element) await element.click(); // Start button. if quiz is partial it wont be there
  }, driver);
  await waitAnimation();
  const wotElement = await driver.findElements(By.className('bt_optionVS'));
  if (wotElement.length > 0) { // check/start whats on top quiz
    await startWhatsOnTop(driver);
    return;
  }
  const quizLength = await getRewardsQuizLength(driver);
  const answersLength = await driver.executeScript('return document.querySelector(\'.btOptions\').children.length').catch(() => { }); // quiz with timer is slightly different so must check
  const answersLengthTimer = await driver.executeScript('return document.querySelector(\'.textBasedMultiChoice\').children.length').catch(() => { }) - 2; // -2 because of the timer and the submit button
  console.log(`${driver.gtp.user}:Rewards quiz length ${quizLength}`);

  for (let index = 0; index < quizLength; index += 1) {
    // whichever is defined
    const answers = generateRandomArray(answersLength || answersLengthTimer);
    for (let i = 0; i < answers.length; i += 1) {
      await consentChecks(driver);
      const choice = `rqAnswerOption${answers[i].toString()}`;
      try {
        await locateElementInteract(By.id(choice), undefined, async (element) => {
          await element.click();
          await waitAnimation();
        }, driver);
      } catch (err) {
        if (err.name === 'TimeoutError') {
          await driver.executeScript('location.reload()');
        } else {
          // Handle other errors
          throw err;
        }
      }

      const correctAnsTimed = await locateElementInteract(By.className('correctAnswer'), 1, (element) => element, driver);
      const correctAns = await locateElementInteract(By.id('rqAnsStatus'), 1, async (element) => {
        if (await driver.executeScript('return window.getComputedStyle(document.querySelector(\'#rqAnsStatus\')).color') === 'rgb(198, 8, 19)') {
          return false;
        }
        return element;
      }, driver);
      if (correctAnsTimed || correctAns) break;
    }
    await waitAnimation();
  }
}

async function checkPoll(driver) {
  await locateElementInteract(By.id('btPollOverlay'), undefined, async (element) => {
    if (element) await startPoll(driver);
  }, driver);
}

async function checkSimpleQuiz(driver) {
  await locateElementInteract(By.className('wk_OuterDivAlignment'), undefined, async (element) => {
    if (element) await startSimpleQuiz(driver);
  }, driver);
}

async function checkForRewardsQuiz(driver) {
  const rewardsQuizElement = await driver.findElements(By.className('rqMenubar'));
  if (rewardsQuizElement.length > 0) {
    await startRewardsQuiz(driver);
    return true;
  }
  const welcomeContainerElement = await driver.findElements(By.id('quizWelcomeContainer'));
  if (welcomeContainerElement.length > 0) {
    await startRewardsQuiz(driver);
    return true;
  }
  return false;
}

async function checkForWhatsOnTopQuiz(driver) {
  const wotElement = await driver.findElements(By.className('bt_optionVS'));
  if (wotElement.length > 0) {
    await startWhatsOnTop(driver);
    return true;
  }
  return false;
}

async function checkForWelcomeTour(driver) {
  await locateElementInteract(By.className('welcome-tour-avaliable-counter'), undefined, async (element) => {
    if (element) {
      console.log('skipping welcome tour');
    }
  }, driver);
  await locateElementInteract(By.className('glyph-cancel'), undefined, async (element) => {
    if (element) {
      console.log('closing welcome tour');
      await element.click();
      await waitAnimation();
    }
  }, driver);
}

export async function checkRewardsQuiz(driver) {
  if (await checkForRewardsQuiz(driver)) {
    return;
  }
  if (await checkForWhatsOnTopQuiz(driver));
}

async function checkLevelBenefits(driver) {
  await locateElementInteract(By.id('level-benefit-link'), undefined, async (element) => {
    if (element) {
      console.log('clicking level benefits');
      await element.click();
    }
  }, driver);
}

export async function checkTask(driver) {
  await checkRewardsQuiz(driver);
  await checkPoll(driver);
  await checkSimpleQuiz(driver);
  await checkForWelcomeTour(driver);
  await checkLevelBenefits(driver);
  await waitAnimation();
}

async function completeTasks(driver) {
  while (driver.gtp.tasks.incomplete.length > 0) {
    console.log(`${driver.gtp.user}:completing task ${driver.gtp.tasks.incomplete.length}`);
    const oldTabLength = await driver.getAllWindowHandles();
    await driver.gtp.tasks.incomplete[0].click();
    const newTabLength = await driver.getAllWindowHandles();
    if (oldTabLength.length < newTabLength.length) await tabGetNew(driver);
    await consentChecks(driver);
    await waitAnimation();
    await checkTask(driver);
    await waitAnimation();
    if (oldTabLength.length < newTabLength.length) await tabClose(driver);
    else await tabGetNew(driver);
    driver.gtp.tasks.incomplete.splice(0, 1);
  }
}

export async function startDashboard(driver) {
  await tabGet(0, driver);
  await waitAnimation();
  if (!driver.gtp.tasks.incomplete.length) return console.log(`${driver.gtp.user}:no tasks`);
  console.log(`${driver.gtp.user}:${driver.gtp.tasks.incomplete.length} incomplete tasks`);
  await waitAnimation();
  await completeTasks(driver);
  await waitAnimation();
  console.log(`${driver.gtp.user}:${driver.gtp.tasks.incomplete.length} incomplete tasks`);
}
