import { getRandomGeo } from './helpers.js';
// should throw error if google trends is down/unavaliable

function checkStatus(res) {
  if (!res.ok) throw new Error('Error fetching google trends');
  return res;
}
// fetch google trends
function fetchTrends(date, geo) {
  return fetch(`https://trends.google.com/trends/api/dailytrends?hl=en-GB&ed=${date}&geo=${geo}&ns=15`)
    .then((res) => checkStatus(res))
    .then((res) => res.text());
}

// parse google trends
function parseTrends(trendText) {
  return JSON.parse(trendText.slice(6, trendText.length))
    .default.trendingSearchesDays[0]
    .trendingSearches;
}

// add related queries to the set
function addRelatedQueries(trendSet, trend) {
  trend.relatedQueries.forEach((relatedTrend) => {
    if (Math.floor(Math.random() * 2) > 0) trendSet.add(relatedTrend.query);
  });
  return trendSet;
}

// unique set of trend queries, small random element, skip adding a trend/related
function addUniqueQueries(parsedTrends, queries) {
  parsedTrends.forEach((trend) => {
    if (Math.floor(Math.random() * 2) > 0) queries.add(trend.title.query);
    addRelatedQueries(queries, trend);
  });
  return queries;
}

// filter queries down to amount
function filterQueries(queriesArray, amount) {
  return queriesArray.filter((_, i) => i < amount);
}

function randBetween(a, b) {
  return Math.floor(Math.random() * b) + a;
}

function daysToMillis(days) {
  return days * 24 * 60 * 60 * 1000;
}

function formatDate(date) {
  const pad = (num) => num.toString().padStart(2, '0');
  return `${date.getFullYear()}${pad(date.getMonth() + 1)}${pad(date.getDate())}`;
}

function trendDate() {
  const date = new Date(Date.now() - daysToMillis(randBetween(1, 28)));
  return formatDate(date);
}

export default async function getQueries(amount) {
  const queries = new Set();
  while (queries.size <= amount) {
    const trends = await fetchTrends(trendDate(), getRandomGeo());
    addUniqueQueries(parseTrends(trends), queries);
  }
  return filterQueries([...queries], amount);
}
