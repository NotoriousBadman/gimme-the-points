import { Builder, Capabilities } from 'selenium-webdriver';
import chrome from 'selenium-webdriver/chrome.js';
import firefox from 'selenium-webdriver/firefox.js';

function createChromeOptions(platform, driverConfig) {
  const options = new chrome.Options();

  options.addArguments('log-level=3');
  options.addArguments('disable-infobars');
  options.addArguments('disable-extensions');
  options.addArguments('disable-notifications');
  options.addArguments('use-fake-device-for-media-stream ');
  options.setUserPreferences({
    'profile.default_content_setting_values.notifications': 2,
    'profile.default_content_setting_values.geolocation': 2,
  });
  if (driverConfig.disable_images) options.setUserPreferences({ 'profile.managed_default_content_settings.images': 2 });
  if (driverConfig.disable_gpu) options.addArguments('--disable-gpu');
  if (platform === 'desktop') options.addArguments(`user-agent=${driverConfig.desktop_user_agent}`);
  else if (platform === 'mobile') options.addArguments(`user-agent=${driverConfig.mobile_user_agent}`);
  if (process.env.HEADLESS || driverConfig.headless) options.headless();
  return options;
}

function createFirefoxOptions(platform, driverConfig) {
  const options = new firefox.Options();
  options.setPreference('permissions.desktop-notification.notNow.enabled', false);
  options.setPreference('permissions.desktop-notification.postPrompt.enabled', false);
  options.setPreference('permissions.default.desktop-notification', 2);
  options.setPreference('dom.webnotifications.enabled', false);
  options.setPreference('dom.webnotifications.silent.enabled', true);
  options.setPreference('permissions.default.geo', 2);
  if (driverConfig.disable_images) options.setPreference('permissions.default.image', 2);
  if (driverConfig.disable_gpu) {
    options.setPreference('layers.acceleration.disabled', true);
    options.setPreference('gfx.direct2d.disabled', true);
  }
  if (platform === 'desktop') options.setPreference('general.useragent.override', driverConfig.desktop_user_agent);
  else if (platform === 'mobile') options.setPreference('general.useragent.override', driverConfig.mobile_user_agent);
  if (process.env.HEADLESS || driverConfig.headless) options.headless();
  return options;
}

function createChromeDriver(platform, driverConfig) {
  const options = createChromeOptions(platform, driverConfig);
  const chromeCapabilities = new Capabilities().set('goog:chromeOptions', {
    args: [
      '--disable-infobars',
      '--use-fake-ui-for-media-stream',
      '--use-fake-device-for-media-stream',
      '--disable-geolocation',
    ],
  });
  let builder = new Builder().withCapabilities(chromeCapabilities).forBrowser('chrome').setChromeOptions(options);
  if (process.env.DOCKER || driverConfig.docker) {
    builder = builder.usingServer(process.env.DOCKER || driverConfig.docker);
  }
  return builder.build();
}

function createFirefoxDriver(platform, driverConfig) {
  const options = createFirefoxOptions(platform, driverConfig);
  let builder = new Builder().forBrowser('firefox').setFirefoxOptions(options);
  if (process.env.DOCKER || driverConfig.docker) {
    builder = builder.usingServer(process.env.DOCKER || driverConfig.docker);
  }
  return builder.build();
}

export default async function createBrowser(platform, driverConfig) {
  console.log('creating driver');
  try {
    if (driverConfig.driver === 'chrome') return createChromeDriver(platform, driverConfig);
    if (driverConfig.driver === 'firefox') return createFirefoxDriver(platform, driverConfig);
  } catch (err) {
    console.log('error creating driver');
    throw err;
  }
}
