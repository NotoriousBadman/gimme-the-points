import { until } from 'selenium-webdriver';
import getQueries from './queries.js';
import { waitAnimation, tabClose, consentChecks } from './helpers.js';

function getNumberOfSearches(stats) {
  if (!stats) return 0;
  const earnedPoints = parseInt(stats.match(/^\d+/)[0], 10);
  const totalPoints = parseInt(stats.match(/\d+$/)[0], 10);
  if (earnedPoints === totalPoints) return 0;
  if (totalPoints === 90 || totalPoints === 60 || totalPoints === 30) {
    return (totalPoints - earnedPoints) / 2.7;
  } if (totalPoints === 150 || totalPoints === 100 || totalPoints === 50) {
    return (totalPoints - earnedPoints) / 4.7;
  }
  return 'Invalid total points';
}

// remove stats param from this function
// eslint-disable-next-line consistent-return
export default async function search(stats, driver) {
  const numberOfSearches = getNumberOfSearches(stats);
  if (!numberOfSearches) return console.log(`${driver.gtp.user}:no searches`);
  const queries = await getQueries(numberOfSearches);
  console.log(`${driver.gtp.user} ${queries}`);
  await driver.switchTo().newWindow('tab');
  await driver.get('https://www.bing.com/');
  await waitAnimation();
  for (let i = 0; i < queries.length; i += 1) {
    await consentChecks(driver); // sign in button can appear
    if (queries[i].indexOf("'")) queries[i] = queries[i].replaceAll("'", "\\'");
    try {
      if (driver.gtp.platform === 'mobile' && i === 10) { // mobile searches have the page refreshed after 10 searches to prevent a long timeout and potential error
        await driver.executeScript('location.reload()');
      }
      const searchField = await driver.findElement({ id: 'sb_form_q' });
      await searchField.clear();
      await searchField.sendKeys(queries[i]);
      await searchField.sendKeys('\uE007');
      await driver.wait(until.stalenessOf(searchField), 60000);
      await waitAnimation();
    } catch (err) {
      if (err.name === 'TimeoutError') {
        const searchField = await driver.findElement({ id: 'sb_form_q' });
        await searchField.clear();
        await searchField.sendKeys(queries[i]);
        await searchField.sendKeys('\uE007');
        await driver.wait(until.stalenessOf(searchField), 60000);
        await waitAnimation();
      } else {
        // Handle other errors
        await tabClose(driver);
        throw err;
      }
    }
    await waitAnimation();
  }
  await tabClose(driver);
}
