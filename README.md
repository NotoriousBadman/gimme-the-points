# Gimme-the-points

Get Microsoft rewards points so you don't have to.

## Getting Started

Download or clone it.

### Prerequisites

What things you need to run the software.

``` Firefox or chrome & Nodejs ```

### Installing

```npm install```

### Running

Enter accounts into accounts.json and run ```npm start```

### Configuration

#### config.json

Set various options for the driver and app.

#### environment variables

```bash
    DRIVER=chrome|firefox
    HEADLESS=true|false
    DOCKER=<selenium-hub>
```
